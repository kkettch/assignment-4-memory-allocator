#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/* выделить регион памяти и инициализировать его блоком */
//addr - адрес выделения памяти
//query - размер памяти в байтах
//возвращаем информацию о выделенном регионе
static struct region alloc_region  ( void const * addr, size_t query ) {
    //общий размер региона = место для заголовка + размер памяти
    size_t common_size = region_actual_size(query + offsetof(struct block_header, contents));
    //выделяем память
    void* region = map_pages(addr, common_size, MAP_FIXED_NOREPLACE);
    //если не получилось, то выделяем в любом месте
    if (region == MAP_FAILED) {
        region = map_pages(addr, common_size, 0);
        //если опять не получилось, то кидаем ошибку
        if (region == MAP_FAILED) return REGION_INVALID;
    }
    //инициализируем блок
    block_init(region, (block_size){common_size}, NULL);
    //и возвращаем структуру
    return (struct region) {
            .addr = region,
            .size = common_size,
            .extends = addr == region
    };
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;
    return region.addr;
}

//проверим что блоки друг за другом
static bool is_block_continuous (struct block_header const* fst, struct block_header const* snd );

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    //указатель на текущий блок
    struct block_header* current = HEAP_START;
    //проходимся по всем блокам
    while (current) {
        //получаем указатель на след блок
        struct block_header* next = current->next;
        //объединение блоков
        while (next && is_block_continuous(current, next)) {
            current->capacity.bytes += next->capacity.bytes;
            next = next->next;
        }
        //освобождение памяти
        munmap(current, offsetof(struct block_header, contents) + current->capacity.bytes);
        current = next;
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//разделяем блок на два если он большой или возвращаем false
static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (!block) return false;
    if (!block_splittable(block, query)) return false;
    //изначальная вместимость блока
    block_capacity old_capacity = block->capacity;
    //нужная вместимость блока
    block->capacity.bytes = query;
    //адрес второго блока
    void *second_addr = block_after(block);
    //инициализация первого блока
    block_init(second_addr, (block_size) {.bytes = old_capacity.bytes - query}, block->next);
    //изменения указателя
    block->next = second_addr;
    return true;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}

static bool is_block_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && is_block_continuous( fst, snd ) ;
}

//объединяем следующий блок с текущим, если это невозможно - false
static bool try_merge_with_next( struct block_header* block ) {
    if (!block || !block->next || !mergeable(block, block->next)) return false;
    //объединение
    block_init(block, (block_size) {.bytes = size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes}, block->next->next);
    return true;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};

//поиск блока заданного размера
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
    struct block_search_result block_res = {.type = BSR_CORRUPTED};
    if (!block) return block_res;
    struct block_header *previous = NULL;
    //проходим по списку блоков
    while (block) {
        // объединение блоков, пока это возможно
        while (try_merge_with_next(block));
        //если блок подходит, то возвращаем
        if (block->is_free && block_is_big_enough(sz, block)) {
            block_res.type = BSR_FOUND_GOOD_BLOCK;
            block_res.block = block;
            return block_res;
        }
        previous = block;
        block = block->next;
    }
    //иначе возвращаем последний блок
    block_res.type = BSR_REACHED_END_NOT_FOUND;
    block_res.block = previous;
    return block_res;
}

///*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
// Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    //поиск блока
    struct block_search_result good_block  = find_good_or_last(block, query);
    //если блок найден, то возвращаем его предварительно разделив его, если надо
    if (good_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(good_block.block, query);
        good_block.block->is_free = false;
        return good_block;
    }
    //все равно возвращаем блок
    return good_block;
}

//увеличиваем размер кучи выделяя новый регион памяти
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if (!last) return NULL;
    //выделяем регион памяти
    struct region region_res = alloc_region(block_after(last), query);
    if (region_is_invalid(&region_res)) return NULL;
    //меняем указатели
    last->next = region_res.addr;
    //если можно объединить регион с предыдущим блоком, то возвращаем указатель
    if (region_res.extends && try_merge_with_next(last))
        return last;
    return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    size_t size_maximum = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result header_res = try_memalloc_existing(size_maximum, heap_start);
    //ну можно и через switch
    switch (header_res.type) {
        case BSR_REACHED_END_NOT_FOUND:
            header_res = try_memalloc_existing(size_maximum, grow_heap(header_res.block, size_maximum));
            if (header_res.type != BSR_FOUND_GOOD_BLOCK) {
                return NULL;
            }
            return header_res.block;

        case BSR_FOUND_GOOD_BLOCK:
            return header_res.block;

        default:
            return NULL;
    }
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    while (try_merge_with_next(header))
        ;
}
