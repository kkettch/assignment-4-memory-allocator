#include <stdio.h>
#include <sys/mman.h>

#include "util.h"
#include "mem.h"
#include "mem_internals.h"

static void test_1() {
    void* heap = heap_init(4096);
    if (!heap) {
        err("Тест 1 провален. Куча не инициализирована.\n");
    }
    void* block = _malloc(1024);
    if (!block) {
        err("Тест 1 провален. Выделение памяти не удалось.\n");
    }
    fprintf(stdout, "Куча после выделения памяти:\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    fprintf(stdout, "Тест 1 пройден успешно!\n");
}

static void test_2() {
    void* heap = heap_init(4096);
    if (!heap) {
        err("Тест 2 провален. Куча не инициализирована.\n");
    }
    void* block1 = _malloc(1024);
    void* block2 = _malloc(1024);
    if (!block1 || !block2){
        err("Тест 2 провален. Память не выделена.\n");
    }
    _free(block1);
    if (!block2) {
        err("Тест 2 провален. Блок поврежден первым блоком.");
    }
    fprintf(stdout, "Блок 1 освобожден:\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    fprintf(stdout, "Тест 2 пройден успешно!\n");
}

static void test_3() {
    void* heap = heap_init(4096);
    if (!heap) {
        err("Тест 3 провален. Куча не инициализирована.\n");
    }
    void* block1 = _malloc(1024);
    void* block2 = _malloc(1024);
    void* block3 = _malloc(1024);
    if (!block1 || !block2 || block3){
        err("Тест 3 провален. Память не выделена.\n");
    }
    _free(block1);
    _free(block2);
    if (!block3) {
        err("Тест 3 провален. 3-ий блок поврежден.\n");
    }
    fprintf(stdout, "Блок 1 и 2 освобождены:\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 4096}).bytes);
    fprintf(stdout, "Тест 3 пройден успешно!\n");
}

static void test_4() {
    void* heap = heap_init(4096);
    if (!heap) {
        err("Тест 4 провален. Куча не инициализирована.\n");
    }
    void* block1 = _malloc(REGION_MIN_SIZE);
    void* block2 = _malloc(1024);
    if(!block1 || !block2) {
        err("Тест 4 провален. Память не выделена.\n");
    }
    struct block_header* header1 = (struct block_header*) (((uint8_t*)block1)-offsetof(struct block_header, contents));
    struct block_header* header2 = (struct block_header*) (((uint8_t*)block2)-offsetof(struct block_header, contents));
    if (!header1 || !header2){
        err("Тест 4 провален. Заголовки пустые\n");
    }
    if (header1->next != header2){
        err("Тест 4 провален. Заголовки не последовательные\n");
        return;
    }

    munmap(heap, ((struct block_header*) heap)->capacity.bytes + header1->capacity.bytes + header2->capacity.bytes);
    fprintf(stdout, "Тест 4 пройден успешно!\n");
}

static void test_5() {
    void* heap = heap_init(4096);
    if (!heap) {
        err("Тест 5 провален. Куча не инициализирована.\n");
    }
    void* block1 = _malloc(REGION_MIN_SIZE);
    if (!block1) {
        err("Тест 5 провален. Память не выделена.\n");
    }
    struct block_header* header_1 = (struct block_header*) (((uint8_t*)block1)-offsetof(struct block_header, contents));
    if (!header_1){
        err("Тест 5 провален. Заголовки пустые\n");
    }
    void* address = header_1->contents + header_1->capacity.bytes;
    void* region_new = mmap(address, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0 );

    void* block2 = _malloc(REGION_MIN_SIZE);
    if (!block2) {
        err("Тест 5 провален. Выделение памяти для блока не удалось\n");
        return;
    }
    struct block_header* header_2 = (struct block_header*) (((uint8_t*)block2)-offsetof(struct block_header, contents));
    if(!header_2){
        err("Тест 5 провален\n");
        return;
    }

    munmap(region_new, size_from_capacity((block_capacity){.bytes = REGION_MIN_SIZE}).bytes);
    munmap(heap, ((struct block_header*) heap)->capacity.bytes + header_1->capacity.bytes + header_2->capacity.bytes);
    fprintf(stdout, "Тест 5 пройден успешно!\n");
}

int main() {
    //Обычное успешное выделение памяти
    printf("Тест 1:\n");
    test_1();

    // Освобождение одного блока из нескольких выделенных.
    printf("Тест 2:\n");
    test_2();

    //Освобождение двух блоков из нескольких выделенных
    printf("Тест 3:\n");
    test_3();

    //Память закончилась, новый регион памяти расширяет старый
    printf("Тест 4:\n");
    test_4();

    //Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
    printf("Тест 5:\n");
    test_5();
}
